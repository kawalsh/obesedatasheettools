# Takes a gargantuan CSV and breaks it up into smaller chunks

import sys

sys.path.append("/modules")

from modules.chunk_it_up import chunk_it_up

def main():

  if(len(sys.argv) != 4):
    print(f"Expected 3 command line arguments, got {len(sys.argv)-1}")
    print("Usage is python chunk_it_up.py <file_name> <datasheet_name> <delimiter>")
    exit()

  file_name = sys.argv[1]
  datasheet_name = sys.argv[2]
  delimiter = sys.argv[3]

  chunk_it_up(file_name, datasheet_name, delimiter)

main()