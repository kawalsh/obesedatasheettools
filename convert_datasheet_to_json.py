import argparse
import json
import sys

sys.path.append("/modules")

from modules.convert_to_json import convert_to_json

def main():

  parser = argparse.ArgumentParser()

  parser.add_argument("--filenames", required=True, metavar="MyDatasheet1.csv,MyDatasheet2.csv", help="The names of the CSV files to read from")
  parser.add_argument("--datasheetname",  required=True, metavar="MyDatasheet", help="The name to give the datasheet")
  parser.add_argument("--seller", required=True, help="The alphanumeric seller ID")
  parser.add_argument("--primarylookup", required=True, help="The column to use for the primary lookup")
  parser.add_argument("--valuelookups", default="", help="A comma separated list of the columns to use for the value lookups")
  parser.add_argument("--rangelookups", default="", nargs="+", help="A comma separated list of the columns to use for the range lookups")
  parser.add_argument("--data", required=True, help="A comma separated list of the columns that contain the data being looked up")
  parser.add_argument("--delimiter", default=",", help="The delimiter used in the source CSV file.  Defaults to a comma.")

  args = parser.parse_args()

  file_names = args.filenames.split(",")
  base_datasheet_name = args.datasheetname
  seller_id = args.seller
  primary_lookup_header = args.primarylookup
  value_lookup_headers = args.valuelookups.split(",") if args.valuelookups != "" else []
  range_lookup_headers = args.rangelookups
  data_headers = args.data.split(",") if args.data != "" else []
  delimiter = args.delimiter

  convert_to_json(file_names, base_datasheet_name, seller_id, primary_lookup_header, range_lookup_headers, value_lookup_headers, data_headers, delimiter)

main()