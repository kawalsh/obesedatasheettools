# Because product price doesn't return a nice error message for overlaps :-(
# This script assumes you've already run remove_the_dupes.py and are running this one on the output of that.

import sys

sys.path.append("/modules")

from modules.find_the_overlaps import find_the_overlaps

def main():

  if(len(sys.argv) != 4):
    print(f"Expected 3 command line arguments, got {len(sys.argv)-1}")
    print("Usage is python find_the_overlaps.py <file_name> <lookup_headers> <delimiter>")
    print("Hint: Lookup headers should both be lists, i.e. mcpsku,quantity,bugs")
    exit()

  file_name = sys.argv[1]
  lookup_headers = sys.argv[2].split(',')
  delimiter = sys.argv[3]

  find_the_overlaps(file_name, lookup_headers, delimiter)

main()