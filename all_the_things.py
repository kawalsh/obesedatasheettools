# This script ties all the other scripts together.  

import argparse
import csv
import json
import math
import os
import sys

sys.path.append("/modules")

from modules.remove_the_dupes import remove_the_dupes
from modules.find_the_overlaps import find_the_overlaps
from modules.chunk_it_up import chunk_it_up
from modules.convert_to_json import convert_to_json
from modules.upload_datasheets import upload_datasheets
from modules.authenticate_user import authenticate
from modules.export_datasheets import export_datasheets
from modules.validate_db_connections import test_db_connections
from modules.verify_local_instances import verify_local_instances

def main():
  parser = argparse.ArgumentParser()

  parser.add_argument("--filename", required=True, metavar="MyDatasheet.csv", help="The name of the CSV file to read from")
  parser.add_argument("--datasheetname",  required=True, metavar="MyDatasheet", help="The name to give the datasheet")
  parser.add_argument("--seller", required=True, help="The alphanumeric seller ID")
  parser.add_argument("--alllookups", default="", help="All lookups, in the order that they appear in the datasheet. Used to check for duplicates")
  parser.add_argument("--primarylookup", required=True, help="The column to use for the primary lookup")
  parser.add_argument("--valuelookups", default="", help="A comma separated list of the columns to use for the value lookups")
  parser.add_argument("--rangelookups", default="", nargs="+", help="A comma separated list of the columns to use for the range lookups")
  parser.add_argument("--data", required=True, help="A comma separated list of the columns that contain the data being looked up")
  parser.add_argument("--delimiter", default=",", help="The delimiter used in the source CSV file.  Defaults to a comma.")
  parser.add_argument("--chunksize", default=0.3, type=float, help="The size, in gigabytes, to split the datasheet up into.")
  parser.add_argument("--uploadenvironment", required=True, help="The environment whose API is being called to upload the datasheet.  Can be local, stg, or prd.")
  parser.add_argument("--targetenvironment", help="The environment the datasheet should ultimately end up in.  Should only be specified when upload environment is 'local'")
  parser.add_argument("--productpricedbpass", help="The database password for the target product price environment.  Optional.")
  parser.add_argument("--modeldbpass", help="The database password for the target model service environment. Optional.")
  parser.add_argument("--dbport", help="The port to use when tunneling to the target environment databases. Optional.")
  parser.add_argument("--checkfordupes", action='store_true', help="If this flag is included the file will be checked for duplicates.")
  parser.add_argument("--checkforoverlaps", action='store_true', help="If this flag is included the file will be checked for overlapping data.")

  args = parser.parse_args()

  file_name = args.filename
  datasheet_name = args.datasheetname
  seller_id = args.seller
  all_lookups = args.alllookups.split(",")
  primary_lookup_header = args.primarylookup
  value_lookup_headers = args.valuelookups.split(",") if args.valuelookups != "" else []
  range_lookup_headers = args.rangelookups
  data_headers = args.data.split(",") if args.data != "" else []
  delimiter = args.delimiter
  chunk_size = round(args.chunksize * 1073741824)
  upload_environment = args.uploadenvironment
  target_environment = args.targetenvironment
  product_price_db_pass = args.productpricedbpass
  model_db_pass = args.modeldbpass
  port = args.dbport
  duplicate_check_requested = args.checkfordupes
  overlap_check_requested = args.checkforoverlaps

  # If we weren't given a CSV file, don't even try
  if not file_name.lower().endswith(".csv"):
    sys.exit(f"Source file must be a CSV.")

  # Check that the source file exists
  if not os.path.isfile(file_name): 
    sys.exit(f"Could not file source file {file_name}.")

  # If there aren't two headers for each pair, bail - the commmand line arguments aren't valid
  for range_lookup_pair in range_lookup_headers:
    range_lookups = range_lookup_pair.split(",")
    range_lookups_length = len(range_lookups)
    if(range_lookups_length != 2):
      sys.exit(f"Expected two headers in each range lookup pair, but found {range_lookups_length}")

  # If we're being asked to check for overlaps, ensure that columns have been specified for the "alllookups" parameter
  if (overlap_check_requested and len(all_lookups) < 1):
    sys.exit(f"--alllookupheaders parameter must be specified in order to check for overlapping data.")

  # Is this a valid upload environment?
  if upload_environment != 'local' and upload_environment != 'stg' and upload_environment != 'prd':
    sys.exit(f"Invalid upload environment {upload_environment}.  Valid options are: local, stg, and prd.")

  if(upload_environment == "local" and target_environment == None):
    sys.exit("Target environment must be specified when upload environment is 'local'")

  # Is this a valid target environment?
  if upload_environment == "local" and target_environment != 'stg' and target_environment != 'prd':
    sys.exit(f"Invalid target environment '{target_environment}'.  Valid options are: 'stg' and 'prd'.")

  if target_environment != None and product_price_db_pass == None:
    sys.exit("Must specify product price database password for target environment.")

  if target_environment != None and model_db_pass == None:
    sys.exit("Must specify model service database password for target environment.")

  if target_environment != None and port == None:
    sys.exit("Must specify database port in order to tunnel into target environment database.")

  # Verify we can actually connect to model and product price databases up front
  if target_environment != None and not test_db_connections(target_environment, product_price_db_pass, model_db_pass, port):
    sys.exit("Unable to connect to target environment database(s).")

  # If uploading to a local environment, verify that model and product price are up
  if upload_environment == "local" and not verify_local_instances():
    sys.exit("Local instances must be running in order to upload to them.")

  # Authenticate the user up front.  If the JWT expires between now and when we actually try to upload the datasheet, we can re-authenticate again
  authentication = authenticate()

  file_name_without_file_type = file_name.replace(".csv", "")

  if duplicate_check_requested:
    print("Filtering duplicates...")
    file_name = remove_the_dupes(file_name, delimiter)
    print("All duplicates filtered.\n")

  if overlap_check_requested:
    has_overlaps = find_the_overlaps(file_name, all_lookups, delimiter)
    if has_overlaps:
      exit(f"File contained overlaps, exiting script. Please see {file_name_without_file_type}_Overlaps.csv")

  # if no overlaps, chunk the file (if necessary)
  chunked_file_names = chunk_it_up(file_name, datasheet_name, delimiter, chunk_size)

  # convert the file(s) to json
  print("Now converting datasheets to JSON...")
  json_file_names = convert_to_json(chunked_file_names, datasheet_name, seller_id, primary_lookup_header, range_lookup_headers, value_lookup_headers, data_headers, delimiter)
  print("Finished converting datasheets to JSON.\n")

  # Create the datasheet in the environment of your choice
  datasheet_responses = upload_datasheets(json_file_names, upload_environment, authentication, duplicate_check_requested)

  print(f"The following datasheets were successfully created in the {upload_environment} environment: ")
  for datasheet_response in datasheet_responses:
    print(datasheet_response)
    print("")

  if upload_environment == "local":
    print("\nExporting datasheets from local database...")
    datasheet_ids = map(lambda response: response["id"], datasheet_responses)
    export_datasheets(datasheet_ids, target_environment, product_price_db_pass, model_db_pass, port)
    print("All datasheets successfully exported!")

  # TODO: PUT IN TRANSFER PRICING OPS REPO

main()