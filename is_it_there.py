# Trying to figure out if a lookup exists in an excessively huge datasheet? This is the script for you.

import csv
import json
import sys

def main():

  if(len(sys.argv) != 4):
    print(f"Expected 2 command line arguments, got {len(sys.argv)-1}")
    print("Usage is python is_it_there.py <file_name> <lookup_headers> <lookup_values>")
    print("Hint: lookup_headers and lookup_values should both be lists, i.e. mcpsku,quantity,bugs CIM-1234,5,crickets")
    exit()

  file_name = sys.argv[1]
  lookup_headers = sys.argv[2].split(',')
  lookup_values = sys.argv[3].split(',')

  # If an equal number of lookup headers and values wasn't passed in, bail now
  lookup_headers_length = len(lookup_headers)
  lookup_values_length = len(lookup_values)

  if lookup_headers_length != lookup_values_length:
    print(f"Number of lookup headers specified was {lookup_headers_length}, but {lookup_values_length} lookup values were provided.")
    exit()

  name = file_name.replace(".csv", "")

  with open(file_name, mode='r', encoding='utf-8-sig') as infile:
    reader = csv.reader(infile)

    # Get the complete column headers
    column_headers = next(reader)

    heart_beat = 0

    new_file_name = f"{name}_matches.csv"

    with open(new_file_name, mode='w', newline='') as outfile:
      writer = csv.writer(outfile, delimiter=',')
      writer.writerow(column_headers)

      for row in reader:
        if heart_beat % 10000 == 0:
          print(f"Reading in row {heart_beat}, still working...")
        datasheet_row = {}
        for column_header, column_value in zip(column_headers, row):
          datasheet_row[column_header] = column_value

        # Take the values for lookup headers and use them to check for a match
        isMatch = True
        for x in range(0, lookup_headers_length):
          expected_value = lookup_values[x]
          actual_value = datasheet_row[lookup_headers[x]]
        
          if actual_value == expected_value:
            isMatch = isMatch and True
          else:
            isMatch = isMatch and False

        if isMatch == True:
          print(f"Found a match at row {heart_beat}!")
          print(row)
          print(lookup_values)
          writer.writerow(row)

        heart_beat = heart_beat + 1

main()