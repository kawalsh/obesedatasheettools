import psycopg2

def test_db_connections(target_environment, product_price_db_pass, model_db_pass, port):
  valid_db_connections = True

  product_price_db = "productprice"
  model_db = "model"

  if target_environment == "stg":
    product_price_db = f"{product_price_db}_stg"
    model_db = f"{model_db}_stg"

  try:
    conn = psycopg2.connect(f"dbname={product_price_db} user=productprice password={product_price_db_pass} host=127.0.0.1 port={port}")
    conn.close()
  except:
    print(f"Unable to connect to {target_environment} product price database")
    valid_db_connections = False

  try:
    conn = psycopg2.connect(f"dbname={model_db} user=model password={model_db_pass} host=127.0.0.1 port={port}")
    conn.close()
  except:
    print(f"Unable to connect to {target_environment} model database")
    valid_db_connections = False

  return valid_db_connections