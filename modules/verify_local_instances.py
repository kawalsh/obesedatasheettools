import requests

def verify_local_instances():
  product_price_available = verify_local_instance("10300", "livecheck", "Product Price")
  model_available = verify_local_instance("5000", "healthcheck", "Model")
  
  return product_price_available and model_available

def verify_local_instance(port, live_check_route, service_name):
  try:
    response = requests.get(f"http://localhost:{port}/{live_check_route}")
  except requests.exceptions.RequestException:
    print(f"Local instance of {service_name} service is not available.")
    return False

  if(response.status_code != 200):
    print(f"Local instance of {service_name} service is not available.")
    return False

  return True