import psycopg2
import re
import subprocess
import sys

def export_datasheets(datasheet_ids, target_environment, product_price_db_pass, model_db_pass, port):
  product_price_db = "productprice"
  model_db = "model"

  if target_environment == "stg":
    product_price_db = f"{product_price_db}_stg"
    model_db = f"{model_db}_stg"

  for datasheet_id in datasheet_ids:
    print(f"Now exporting data for datasheet {datasheet_id}...")

    datasheet_information_sql = f"SELECT * FROM datasheetinformation WHERE id='{datasheet_id}'"

    # select from datasheet_information in product price so we can get the model service datasheet ID
    conn = psycopg2.connect("dbname=productprice user=postgres password=local")
    cur = conn.cursor()
    cur.execute(datasheet_information_sql)
    datasheet_information = cur.fetchone()
    internal_datasheet_id = datasheet_information[1]

    cur.close()
    conn.close()

    datasheet_header_sql = f"SELECT * FROM datasheetheader WHERE datasheetid='{internal_datasheet_id}'"
    datasheet_body_sql = f"SELECT * FROM datasheetbody WHERE datasheetid='{internal_datasheet_id}'"

    print("Exporting from the datasheetinformation table...")
    export_data(datasheet_information_sql, "datasheetinformation", "productprice", product_price_db, "productprice", product_price_db_pass, port)

    print("Exporting from the datasheetheader table...")
    export_data(datasheet_header_sql, "datasheetheader", "model", model_db, "model", model_db_pass, port)

    print("Exporting from the datasheetbody table...")
    export_data(datasheet_body_sql, "datasheetbody", "model", model_db, "model", model_db_pass, port)

    print(f"Finished exporting data for datasheet {datasheet_id}.\n")

def export_data(sql, table_name, from_db, to_db, db_user, db_pass, port):
  export_psql_cmd = f"SET PGPASSWORD=local&& psql -U postgres -h 127.0.0.1 --command \"copy ({sql}) TO stdout with csv\" {from_db}"
  import_psql_cmd = f"SET \"PGPASSWORD={db_pass}\"&&psql -U {db_user} -h 127.0.0.1 -p {port} -c \"copy {table_name} FROM stdin CSV\" {to_db}"

  process = subprocess.Popen(f"{export_psql_cmd} | ({import_psql_cmd})", shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
  
  output = process.communicate()[0]
  stdout = output.decode('ascii').rstrip()

  if(process.returncode != 0):
    sys.exit(f"Error! Unable to export data from {table_name} table: {stdout}")
  else:
    number_of_rows = re.match(r"COPY (\d+)", stdout).group(1)
    print(f"{number_of_rows} rows copied.")