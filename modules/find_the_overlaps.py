# Because product price doesn't return a nice error message for overlaps :-(
# This script assumes you've already run remove_the_dupes.py and are running this one on the output of that.

import csv
import json
import sys

def find_the_overlaps(file_name, lookup_headers, delimiter):
  print(f"Checking {file_name} for overlaps...")
  name = file_name.replace(".csv", "")

  overlaps_file_name = f"{name}_Overlaps.csv"

  with open(file_name, mode='r', encoding='utf-8-sig') as infile:
    reader = csv.reader(infile, delimiter=delimiter)

    # Get the column headers
    column_headers = next(reader)

    has_overlaps = False
    dupe_row_check = {}
    heart_beat = 0

    for row in reader:
      if heart_beat % 10000 == 0:
        print(f"Reading in row {heart_beat}, still working...")
      datasheet_row = {}
      for column_header, column_value in zip(column_headers, row):
        datasheet_row[column_header] = column_value

      # Take the values for lookup headers and use them to check for dupes
      lookup_values = ""
      for lookup_header in lookup_headers:
        # Check to see if the lookup header is there and gracefully exit if it's not
        if not lookup_header in datasheet_row:
          sys.exit(f"Could not find value for {lookup_header} in row {heart_beat}")

        lookup_values = lookup_values + datasheet_row[lookup_header] + ","
    
      if lookup_values not in dupe_row_check:
        dupe_row_check[lookup_values] = row
      else:
        print(f"Found overlapping data at row {heart_beat}...")
        with open(overlaps_file_name, mode='w', newline='') as outfile:
          writer = csv.writer(outfile, delimiter=delimiter)

          # If this the first time we're seeing overlaps, write the column headers
          if not has_overlaps:
            writer.writerow(column_headers)
            has_overlaps = True

          writer.writerow(row)
          writer.writerow(dupe_row_check[lookup_values])

      heart_beat = heart_beat + 1

  return has_overlaps