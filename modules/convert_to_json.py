import csv
import json
import sys

datasheet_json = {
  "seller": {
    "sellerType": "fulfillers",
    "sellerId": "hjh8nrhpqx"
  },
  "name": "Example Datasheet",
  "primaryLookupHeader": "McpSku",
  "rangeLookupHeaders": [],
  "valueLookupHeaders": [
    "Substrate"
  ],
  "dataHeaders": [
    "SubstratePrice",
    "SetupPrice"
  ],
  "rows": []
}

def convert_to_json(csv_file_names, base_datasheet_name, seller_id, primary_lookup_header, range_lookup_headers, value_lookup_headers, data_headers, delimiter):

  json_file_names = []

  file_count = 1

  for csv_file_name in csv_file_names:
    datasheet_name = base_datasheet_name if len(csv_file_names) == 1 else f"{base_datasheet_name}_{file_count}"

    datasheet_json["name"] = datasheet_name
    datasheet_json["seller"]["sellerId"] = seller_id
    datasheet_json["primaryLookupHeader"] = primary_lookup_header
    datasheet_json["valueLookupHeaders"] = value_lookup_headers
    datasheet_json["dataHeaders"] = data_headers

    # Range lookup headers now come in as pairs of comma separated strings
    for range_lookup_pair in range_lookup_headers:
      range_lookups = range_lookup_pair.split(",")
      range_lookup_header = {
        "lowerBound": range_lookups[0],
        "upperBound": range_lookups[1]
      }

      datasheet_json["rangeLookupHeaders"].append(range_lookup_header)

    # If the datasheet is large enough, we won't be able to hold the whole json structure in memory.
    # Luckily the rows are at the very end of the datasheet request body, so we can just
    # write the first bit, and then append the rows and the ]} at the end
    datasheet_json_string = str(json.dumps(datasheet_json))
    datasheet_json_string = datasheet_json_string[:-2]

    json_file_name = f"{datasheet_name}.json"
    json_file_names.append(json_file_name)

    f = open(json_file_name, "w")
    f.write(datasheet_json_string)

    with open(f"{csv_file_name}", mode='r', encoding='utf-8-sig') as infile:
      reader = csv.reader(infile, delimiter=delimiter)

      # Get the column headers
      column_headers = next(reader)

      heart_beat = 0

      for row in reader:
        if heart_beat % 10000 == 0:
          print(f"Reading in row {heart_beat} for file {csv_file_name}, still working...")
        datasheet_row = {}
        for column_header, column_value in zip(column_headers, row):
          datasheet_row[column_header] = column_value

        f.write(json.dumps(datasheet_row))
        f.write(",")

        heart_beat = heart_beat + 1

    f.write("]}") 
    f.close()

    file_count += 1

  return json_file_names