import gzip
import io
import requests
import stdiomask
import sys
import time

sys.path.append(".")

from modules.authenticate_user import reauthenticate

def upload_datasheets(file_names, environment, authentication, duplicates_checked):
  datasheet_responses = []
  number_of_files = len(file_names)
  current_file = 1
  for file_name in file_names:
    # In the event that we're uploading multiple large datasheets, it's possible a user's JWT could expire between uploads
    # Don't hassle the user if they have an expired JWT - just reauthenticate for them.
    authentication = check_for_expired_auth(authentication)

    print(f"Compressing datasheet {file_name}...")
    compressed = compress_datasheet(file_name)
    print("Finished compressing datasheet.")

    url = ""
    ignore_duplicates = "?ignoreDuplicates=true" if duplicates_checked else ""

    if environment == "local":
        url = f"http://localhost:10300/api/v2/datasheets{ignore_duplicates}"
    elif environment == "stg":
      url = f"https://stg-productprice.ff.cimpress.io/api/v2/datasheets{ignore_duplicates}"
    elif environment == "prd":
      url = f"https://productprice.ff.cimpress.io/api/v2/datasheets{ignore_duplicates}"
    else:
      sys.exit(f"Error! Provided environment was ${environment}.  Valid options for <environment> are: local, stg, and prd.")

    access_token = authentication["access_token"]
    headers = {
      "authorization": f"Bearer {access_token}",
      "content-encoding": "gzip", 
      "content-type": "application/json"
    }

    print(f"Sending datasheet request {current_file} of {number_of_files} to product price...")

    response = requests.post(url, headers=headers, data=compressed)
    status_code = response.status_code

    if status_code == 201:
      print("Datasheet successfully created.\n")
      datasheet_responses.append(response.json())
      current_file += 1
    elif status_code == 503 or status_code == 502:
      sys.exit("Error! Datasheet creation failed due to timeout.")
    else:
      sys.exit(f"Error! Datasheet creation failed due to an unknown error. Status code was: {status_code}")

  return datasheet_responses

def compress_datasheet(file_name):
  with open(file_name, "rb", buffering=0) as file_input:
    return gzip.compress(file_input.read())

def check_for_expired_auth(authentication):
  current_time = time.time()
  if (current_time >= authentication["expiration_date"]):
    authentication = reauthenticate(authentication)
  
  return authentication