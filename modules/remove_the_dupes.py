# This reads a file in and writes it back out again, but minus the duplicates

import csv
import json

def remove_the_dupes(file_name, delimiter):
  print(f"Checking {file_name} for duplicates...")
  
  with open(file_name, mode='r', encoding='utf-8-sig') as infile:
    reader = csv.reader(infile, delimiter=delimiter)

    # Get the column headers
    column_headers = next(reader)

    dupe_row_check = set([])
    heart_beat = 0
    
    datasheet_name = file_name.replace(".csv", "")
    new_file_name = f"{datasheet_name}_No_Dupes.csv"

    with open(new_file_name, mode='w', newline='') as outfile:
      writer = csv.writer(outfile, delimiter=delimiter)
      writer.writerow(column_headers)

      for row in reader:
        if heart_beat % 10000 == 0:
          print(f"Reading in row {heart_beat}, still working...")
        datasheet_row = {}
        for column_header, column_value in zip(column_headers, row):
          datasheet_row[column_header] = column_value

        # Check to see if a row like this already exists, if so, don't add it
        row_hash = json.dumps(datasheet_row, sort_keys=True)
        
        if row_hash not in dupe_row_check:
          writer.writerow(row)
          dupe_row_check.add(row_hash)
        else:
          print("Skipping duplicate row...")

        heart_beat = heart_beat + 1

  return new_file_name