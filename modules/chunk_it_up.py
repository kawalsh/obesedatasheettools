# Takes a gargantuan CSV and breaks it up into smaller chunks

import csv
import json
import math
import os

def chunk_it_up(file_name, datasheet_name, delimiter, max_chunk_size_in_bytes):
  file_size = os.stat(file_name).st_size

  file_names = []

  # Check the size of the file.  If it's above our max chunk size, chunk that bad boy up.
  print("Now checking to see if datasheet needs to be split up into chunks...")
  if file_size > max_chunk_size_in_bytes:
    # Do some math to figure out the number of chunks
    chunk_count = math.ceil(file_size/max_chunk_size_in_bytes)

    print(f"File will be split up into {chunk_count} chunks.")

    # Get the count of the number of rows in the datasheet
    with open(file_name, mode='r', encoding='utf-8-sig') as infile:
      reader = csv.reader(infile, delimiter=delimiter)

      row_count = sum(1 for row in reader)

      print(f"This CSV has {row_count} rows in it.")

      # Do some math to figure out how many rows should go into each chunk
      chunk_row_count = math.ceil(row_count/chunk_count)

      print(f"Each chunk will have a maximum of {chunk_row_count} rows in it.")

      # Go back to the beginning of the file
      infile.seek(0)

      # Grab the headers, we'll need to put them in every file
      column_headers = next(reader)

      current_row_count = 0
      current_chunk_count = 0
      current_chunk_file_name = ""

      while current_row_count < row_count - 1:
        for row in reader:
          # Let the user know we're not dead yet
          if current_row_count % 10000 == 0:
            print(f"Reading in row {current_row_count}, still working...")

          # If we've reached the end of a chunk, open up a new file
          if current_row_count % chunk_row_count == 0:
            current_chunk_count = current_chunk_count + 1

            current_chunk_file_name = f"{datasheet_name}_Part_{current_chunk_count}.csv"
            print(f"Opening new file {current_chunk_file_name}")

            file_names.append(current_chunk_file_name)
            
            # Write the headers
            with open(current_chunk_file_name, mode='w', encoding='utf-8-sig', newline='') as outfile:
              writer = csv.writer(outfile, quotechar=delimiter, quoting=csv.QUOTE_MINIMAL, delimiter=delimiter)
              writer.writerow(column_headers)

          with open(current_chunk_file_name, mode='a', encoding='utf-8-sig', newline='') as outfile:
            writer = csv.writer(outfile, quotechar=delimiter, quoting=csv.QUOTE_MINIMAL, delimiter=delimiter)
            writer.writerow(row)

          current_row_count = current_row_count + 1
  else: 
    file_names.append(file_name)
    print(f"File is under {max_chunk_size_in_bytes} bytes, no chunking is necessary :)\n")

  return file_names