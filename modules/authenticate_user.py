import requests
import stdiomask
import sys
import time

def authenticate():
  user_info = get_user_name_and_pass()

  return authenticate_user(user_info)

def reauthenticate(authentication):
  return authenticate_user(authentication["user_info"])

def authenticate_user(user_info):
  headers = { 'Content-Type': 'application/json' }

  data = {
    'username': user_info["username"],
    'password': user_info["password"],
    'grant_type': 'http://auth0.com/oauth/grant-type/password-realm',
    'audience': 'https://api.cimpress.io/',
    'scope': 'openid email',
    'client_id': 'ST0wwOc0RavK6P6hhAPZ9Oc2XFD2dGUF',
    'realm': 'cimpresscom-native-waad',
  }

  auth_response = requests.post('https://cimpress.auth0.com/oauth/token', json=data, headers=headers)
  
  while(auth_response.status_code != requests.codes.ok):
    error_description = auth_response.json()['error_description']
    print(f'Error! Could not authenticate user. {error_description}')

    # 99% of the time someone has just typed their password in wrong.  
    # Take pity on them and don't boot them out of the script.
    user_info = get_user_name_and_pass()

    data["username"] = user_info["username"]
    data["password"] = user_info["password"]

    auth_response = requests.post('https://cimpress.auth0.com/oauth/token', json=data, headers=headers)

  auth_response_body = auth_response.json()

  expiration_date = time.time() + auth_response_body["expires_in"]

  return { "user_info": user_info, "access_token": auth_response_body["access_token"], "expiration_date": expiration_date}

def get_user_name_and_pass():
  username = input('ADFS Login (username@cimpress.com): ')
  password = stdiomask.getpass('Password: ')

  return { "username": username, "password": password}