# BigDatasheetTools

Scripts to deal with big datasheets that can't be uploaded via the Pricing Manager.

### Do All the Things ###

While all the scripts can be run individually, this is probably the one you want, as it chains the rest of them together.
It's now possible to toggle via flags whether to check for overlaps and filter out duplicates. See the usage section for more details.
If this is a WMD datasheet, you should probably leave those flags on, as they often give us exports from their catalog with duplicates in them.

If all flags are toggled, the order of operations is as follows:
  1) Remove any duplicate rows. If any are found, they will simply be filtered out, and the script will continue.
  2) Check for overlapping data. If any overlaps are found, they will be written to a file named <file_name>_No_Dupes_Overlaps.csv for review, and the script will exit.
  3) Break the file up into chunks.  If the file is small enough, this step will be skipped.
  4) Convert the file(s) into datasheet JSON.
  5) Upload said files to the specified product price environment.  (Local, stage, or prod)
  6) If the environment the files were uploaded to is a local one, copy the data from your local databases to an environment of your choosing.

Usage: python all_the_things.py --filename <file_name> --datasheetname <datasheet_name> --seller <seller_id> --primarylookup <primary_lookup_header> --valuelookups <value_lookup_headers> --rangelookups <range_lookup_headers> --data <data_headers> --allookups <all_lookup_headers> --delimiter <delimiter> --chunksize <chunk_size> --targetenvironment local --checkfordupes --checkforoverlaps

Arguments:

--filename          The name of the CSV file to create a datasheet from. Required. 
--datasheetname     The name to give the datasheet. Required. 
--seller            The alphanumeric fulfiller ID this datasheet will be created under. Required.
--alllookups        All lookup columns, in the order that they appear in the datasheet.  Optional if you're not checking for overlaps.
--primarylookup     The column to use for the primary lookup.  Required.
--valuelookups      A comma separated list of the columns to use for the value lookups.  Optional if this datasheet has no value lookups.
--rangelookups      A comma separated list of the column pairs to use for the range lookups, in the form of [lowerbound,upperbound otherlowerbound,otherupperbound]. 
                    Optional if this datasheet has no range lookups.
--data              A comma separated list of the columns that contain the data being looked up.  Required.
--delimiter         The delimiter used in the source CSV file. Defaults to a comma. Optional.
--chunksize         The size, in gigabytes, to split the CSV up into. Optional.
--uploadenvironment The environment to upload the datasheet too.  Must be local, stg, or prd. Required.
--targetenvironment The environment the datasheet should ultimately end up in.  Optional. Should only be specified if upload environment is 'local'
--dbport            The port to use to tunnel to the target environment's database.  Optional. 
--checkfordupes     If included, this file will be checked for duplicates.  Optional.
--checkforoverlaps  If included, this file will be checked for overlapping data. Optional.

Example: 
python all_the_things.py --filename PBS_Brochures_8_25_2020.csv --datasheetname PBS_Brochures_8_25_2020 --seller k5h61w79ed --alllookups Format,CoverSubstrateThickness,PageSubstrateThickness,PrintColor,Grammage,BindingType,CoverMaterial,CoverGrammage,CoverPrintColor,PrintingProcess,HolePunching,NumberOfPages,CoverTypes,BindingEdge,CustomWidth,CustomHeight,Material,BindingColor,CoverCoating,CoverLamination,CoverSpecialFinishing,PrintOrientation,CoverForBottomEdge,DesignCount,quantity,min_amount,max_amount --primarylookup Format --valuelookups CoverSubstrateThickness,PageSubstrateThickness,PrintColor,Grammage,BindingType,CoverMaterial,CoverGrammage,CoverPrintColor,PrintingProcess,HolePunching,NumberOfPages,CoverTypes,BindingEdge,CustomWidth,CustomHeight,Material,BindingColor,CoverCoating,CoverLamination,CoverSpecialFinishing,PrintOrientation,CoverForBottomEdge,DesignCount,quantity --rangelookups min_amount,max_amount --data min_price,standard_price,nextday_price,express_price,overnight_price,setup_price --delimiter ; --chunksize 0.3 --uploadenvironment local --targetenvironment stg --dbport 56789 --checkfordupes --checkforoverlaps

Requirements:  

- You will need the python modules `stdiomask`, `requests`, and `psycopg2` installed. (Try `pip install stdiomask requests psycopg2`)
- If the datasheet is big enough that you need to upload to a local environment, you'll need the `big-datasheets` branches of the product price and model services pulled down, as well as a local PostgreSQL install.
- You'll need the `psql` command available in your path.  (Usually it lives here: `C:\Program Files\PostgreSQL\9.6\bin`)

### Removing Duplicates ###

This is meant to strip duplicates from CSVs that are too big to open in Excel.
This script will write the CSV back out again to a new location, minus the duplicates.
The new location will be <file_name>_No_Dupes.csv.  

This is strictly for removing duplicate rows - it will not detect overlaps.
Check out the script for finding overlaps if you want to do that.

Usage is python remove_the_dupes.py <file_name> <delimiter>

### Finding Overlaps ###

This is meant for finding overlaps in a CSV.  If there are duplicates, it'll find those too,
so you probably want to strip those out first to reduce the output of this script, which will
write any overlapping rows out to a file called <file_name>Overlaps.csv.

This is useful for when we're giving large datasheets by end users which are not valid due to
overlapping values, but the datasheet is too big to open in Excel and find the overlaps.

Usage is python find_the_overlaps.py <file_name> <lookup_headers> <delimiter>
The lookup headers should be in the form of a comma separated list, i.e. "mcpsku,min_quantity,max_quantity".

### Splitting the CSV into chunks ###

If a CSV is above a certain size (somewhere between .2 and .4 gigabytes) it will be too big to upload to even a local instance of product price.
This script will break the CSV up into smaller pieces.

Usage is python chunk_it_up.py <file_name> <datasheet_name> <delimiter> <max_chunk_size_in_bytes>

### Converting the datasheet to JSON ###

In order to upload the datasheet to the product price API, it'll need to be in JSON. 
This script turns a CSV into JSON.  You can skip straight to using this script if you know
your datasheet doesn't have duplicates or overlaps, and doesn't need to be split up into chunks.  

If the CSV has already been chunked up and you have more than one file to convert, you can pass in a comma separated list of file names, and it will convert them all to their own JSON file.

Usage is python convert_datasheet_to_json.py --filenames <file_names> --datasheetname <datasheet_name> --seller <seller_id> --primarylookup <primary_lookup_header> --valuelookups <value_lookup_headers> --rangelookups <range_lookup_headers> --data <data_headers> --delimiter <delimiter>

Example: 
python convert_datasheet_to_json.py --filenames PBS_Flat_and_Folded_Flyer1.csv,PBS_Flat_and_Folded_Flyer2.csv --datasheetname PBS_Flat_and_Folded_Flyer --seller k5h61w79ed --primarylookup Format --valuelookups Material,CustomWidth,CustomHeight,Grammage,PrintColor,PrintingProcess,Lamination,Coating,CutShape,HolePunching,SpecialFinishing,BundleCount,Perforation,Fragrance,Folding,FoldingOrientation,PrintOrientation,amount --rangelookups min_amount,max_amount min_quantity,max_quantity --data standard_price,nextday_price,express_price,overnight_price,setup_price,standard_cost,nextday_cost,express_cost,overnight_cost --delimiter ;

### Uploading the datasheet ###

This takes the JSON created by conversion script and uploads it to product price.

Usage is node upload_datasheet.js <file_name> <environment>
Valid options for <environment> are: local, stage, and prod

If you get a gateway timeout, you can try pointing it to a local instance of product price + model service
and then importing the data from your local db directly into prod 🙃

If you run out of heap space, remember the magic incantation:
SET NODE_OPTIONS=--max_old_space_size=8096