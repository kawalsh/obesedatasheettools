# This reads a file in and writes it back out again, but minus the duplicates

import os
import sys

sys.path.append("/modules")

from modules.remove_the_dupes import remove_the_dupes

def main():
  if(len(sys.argv) != 3):
    print(f"Expected 3 command line arguments, got {len(sys.argv)-1}")
    print("Usage is python remove_the_dupes.py <file_name> <delimiter>")
    exit()

  file_name = sys.argv[1]
  delimiter = sys.argv[2]

  remove_the_dupes(file_name, delimiter)

main()