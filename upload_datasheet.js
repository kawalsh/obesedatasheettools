const chalk    = require('chalk');
const fetch    = require('node-fetch');
const fs       = require('fs');
const inquirer = require('inquirer');
const rp       = require('request-promise');
const zlib     = require('zlib');
const { exit } = require('process');

function compressBody(input) {
  console.log("Compressing datasheet...")
  return new Promise(function (resolve, reject) {
    var gzip = zlib.createGzip();
    var compressed = input.pipe(gzip);


    var buffers = []
    compressed.on('data', chunk => {
      buffers.push(chunk);
    });

    compressed.on('end', x => {
      var buffer = Buffer.concat(buffers);
      resolve(buffer);
      console.log("Finished compressing datasheet...");
    });

  });
}

function promptForCredentials() {
  const questions = [
    {
      name: 'username',
      type: 'input',
      message: 'ADFS Login (username@cimpress.com)',
      prefix: chalk.green('[?]'),
    },
    {
      name: 'password',
      type: 'password',
      message: 'Password',
      mask: '*',
      prefix: chalk.green('[?]'),
    },
  ];

  return inquirer.prompt(questions);
}

function userRequestAccess(username, password) {
  const headers = { 'Content-Type': 'application/json' };
  const uri = 'https://cimpress.auth0.com/oauth/token';
  const client_id = 'ST0wwOc0RavK6P6hhAPZ9Oc2XFD2dGUF';
  const grant_type = 'http://auth0.com/oauth/grant-type/password-realm';
  const audience = 'https://api.cimpress.io/';
  const scope = 'openid email';
  const realm = 'cimpresscom-native-waad';

  const body = {
    username,
    password,
    grant_type,
    audience,
    scope,
    client_id,
    realm,
  };

  return rp({
    uri,
    headers,
    body,
    method: 'POST',
    json: true
  });
}

async function go() {
  var args = process.argv.slice(2);

  if(args.length != 2) {
    console.log('Error! Usage is upload_datasheet.js <file_name> <environment>');
    console.log('Possible options for <environment> are: local, stage, and prod.')
    process.exit(1);
  }

  const fileName = args[0];
  const environment = args[1];

  var url = '';

  switch(environment) {
    case 'local':
      url = 'http://localhost:10300/api/v2/datasheets?ignoreDuplicates=true';
      break;
    case 'stage':
      url = 'https://stg-productprice.ff.cimpress.io/api/v2/datasheets?ignoreDuplicates=true'
      break;
    case "prod":
      url = 'https://productprice.ff.cimpress.io/api/v2/datasheets?ignoreDuplicates=true';
      break;
    default:
      console.log(`Error! Provided environment was ${environment}.  Valid options for <environment> are: local, stg, and prod.`);
      process.exit(1);
  }

  const credentials = await promptForCredentials();
  const { username, password } = credentials;
  const auth_response = await userRequestAccess(username, password);

  var inp = fs.createReadStream(fileName);

  var compressed = await compressBody(inp);

  console.log("Sending the datasheet request to product price...")

  var response = await fetch(url, {
    method: 'POST',
    headers: { 'authorization': `bearer ${auth_response.access_token}`, 'content-encoding': 'gzip', 'content-type': 'application/json' },
    body: compressed
  });

  var status = response.status;
  var body = await response.text();

  switch(status) {
    case 201:
      console.log("Datasheet was sucessfully created! :)");
      console.log(JSON.parse(body));
      break;
    case 503:
      console.log('Error! Datasheet creation was unsuccessful due to a timeout.');
      break;
    case 502:
      console.log('Error! Datasheet creation was unsuccessful due to a timeout.');
      break;
    default:
      console.log(`Error! Datasheet creation was unsuccessful due to an unknown error. Status code was ${status}`);
      try {
        console.log(JSON.parse(body));
      } catch(err) {
        console.log(body);
      }
  }
}

(async () => await go())();